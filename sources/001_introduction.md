# Introduction
This book is the primary specification for Mokka. It consists of three different
kinds of material:

* Chapters that informally describe a topic and its usage;
* Chapters that informally describe runtime services, debugging tools and so on;
* Chapters that formally describe the semantics of Mokka and its AST.

This book attemps to be exact and complete. However, this book mostly uses the
English language in favor of formal specifications. This makes the book more
understandable and more fun to read, but it leaves slight room for ambiguity.

This book does not specify any implementation details, since such an
implementation does not exist at the time of writing this.

## Typesetting conventions
This book has several conventions on how it displays information. These
conventions include:

* Statements that define a new term are in _italics_.
* Technical names, such as function names, are typestted as `inline code blocks`.
* Notes are in blockquotes and start with the word "Node:" in **bold**.

# Contributing
Be bold and contribute to this book! You can contribute by opening an issue or
by sending a pull request to [the Mokka repository](https://gitlab.com/mokkalang/spec).

# Compiling the book
This book can be compiled using the supplied Makefile. It uses Pandoc to
convert the Markdown files to \LaTeX, which gets compiled to a PDF.

Any Markdown files in the `sources` directory is automatically linked during
the compilation. The files are linked in alphabetical order.
